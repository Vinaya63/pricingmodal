var slider = document.getElementById("myRange");
var plans = document.getElementsByClassName("plan");

slider.oninput = function () {
    for (var i = 0; i < plans.length; i++) {
        plans[i].classList.remove("active");
    }
    if (this.value <= 10) {
        plans[0].classList.add("active");
    } else if (this.value <= 20) {
        plans[1].classList.add("active");
    } else {
        plans[2].classList.add("active");
    }
};

$(document).ready(function () {
    // Set plan name in modal when order button is clicked
    $(".btn-block").click(function () {
        var plan = $(this).data("plan");
        $("#plan").val(plan); // set the plan value in the form
        // Update the form fields based on the selected plan
        switch (plan) {
            case "Basic":
                // Update the form fields for the Basic plan
                $("#generalModal").find("#name").val("");
                $("#generalModal").find("#email").val("");
                $("#generalModal").find("#comments").val("");
                break;
            case "Pro":
                // Update the form fields for the Pro plan
                $("#generalModal").find("#name").val("");
                $("#generalModal").find("#email").val("");
                $("#generalModal").find("#comments").val("");
                break;
            case "Enterprise":
                // Update the form fields for the Enterprise plan
                $("#generalModal").find("#name").val("");
                $("#generalModal").find("#email").val("");
                $("#generalModal").find("#comments").val("");
                break;
            default:
                // Clear the form fields if no plan is selected
                $("#generalModal").find("#name").val("");
                $("#generalModal").find("#email").val("");
                $("#generalModal").find("#comments").val("");
                break;
        }
    });

    const form = document.querySelector("#my-form");

    function storeData(token, data) {
        // Store the data associated with the token on the server
        // For example, you could use a database or a server-side session
        // Here, we're just storing the data in a global object for simplicity
        window.formResponses = window.formResponses || {};
        window.formResponses[token] = data;
    }

    form.addEventListener("submit", function (event) {
        event.preventDefault();

        const name = document.querySelector("#name").value;
        const email = document.querySelector("#email").value;
        const description = document.querySelector("#comments").value;

        // Generate a unique token for this form submission
        const token = Math.random().toString(36).substr(2, 10);

        // Store the data associated with the token on the server
        storeData(token, { name, email, description });

        // Build the URL with the token instead of the actual data
        const buildUrl = `https://forms.maakeetoo.com/newform/?token=${token}`;

        // Redirect the user to the destination website
        window.location.href = buildUrl;

        // Set the value of the name input field
        document.querySelector("#id_form_name").value = "";
        // Set the value of the email input field
        document.querySelector("#id_email").value = "";
        // Set the value of the comment input field
        document.querySelector("#id_form_description").value = "";
    });
});